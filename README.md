#elogin
Elogin is a simple login system that allows you to not have to worry about the login system for your node.js app, and instead focus on the meat of it. 

##Example of creating a user (in express)

```
var elogin = require('elogin');
	
app.post('/api/user', function(req, res) {
	elogin.add(req.param('username'), req.param('password'), function(result) {
		res.send(result);
	})
});
```

On complete the request will return something along the lines of `{status: 0, response: hcwool}`.

##Example of logging in a user (in express)

```
app.post('/api/user/login', function(req, res) {
	elogin.check(req.param('username'), req.param('password'), function(result) {
		if(result.status == 0) {
			req.session.user = result.response;
		}
		res.send(result);
	});
});
```

Now when you need to know if the user is logged in, simply check if `req.session.user != null`.

##Config
*Note: You do not need to change the config, but it is recommended you do so.*
Currently you can edit these values:

```
{
	url 		: 'Url of the mongo database',
	collection 	: 'The collection which the users are stored in',
	iterations 	: 'How many iterations the crypto process goes through',
	salt 		: 'The salt that is used in the crypto process',
}
```

To change the config all you have to do is make an object with the options you want to change in it, and then pass it through to `elogin.config()`

```
elogin.config({iterations: 7, salt: 'We_L0v3_Ic3-k4R3Am'})
```


##Thank you
Thank you to [runvnc](https://npmjs.org/~runvnc) for the source of inspiration of this project, his comparible login system [login-mongo](https://npmjs.org/package/login-mongo)



