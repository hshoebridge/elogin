// 
//	ELOGIN
//		The simple, yet secure authentication system for express and mongoose.
//

module.exports =  function() {
	var mongoose = require('mongoose');	
	var CryptoJS = require('crypto-js');

	var DB_URL = "mongodb://localhost:27017/users";
	var COLLECTION = "users";
	var SALT = "AbCdeFG";
	var ITERATIONS= 11;

	var schema = mongoose.Schema({
		username: String,
		password: String
	});

	var User = mongoose.model(COLLECTION, schema);

	// The encryption function, found in soupal.
	//	shaEncrypt(XXXX);
	shaEncrypt = function(password) {
		console.log(password);
		var saltedpassword = SALT + password;
		for (var i = 0; i < ITERATIONS - 1; i++) {
			saltedpassword = CryptoJS.SHA256(saltedpassword).toString(CryptoJS.enc.Hex);
		}
		saltedpassword = CryptoJS.SHA256(saltedpassword);
		return saltedpassword.toString(CryptoJS.enc.Base64);
	}

	return {

		// Sets up databases and connections
		// setup()
		setup: function() {
			mongoose.createConnection(DB_URL)
			var db = mongoose.connection;
			db.on('error', console.error.bind(console, '[ELOGIN] connection error: '));
			return this;
		},
		
		// Add literally adds a user
		// add(hcwool, password, function(response) {});
		add: function($username, $password, callback) {
			User.findOne({username: $username}, function(err, user) {
				if(!err) {
					if(user == (void 0)) {
						var securePassword = shaEncrypt($password);
						User.create({username: $username, password: securePassword}, function(err, user) {
							if(!err) {
								callback({status: 0, response: user.username});
							}else {
								callback({status: 1, response: 'database error'});
							}
						});
					}else {
						callback({status: 1, response: 'user exists'})
					}
				}else {
					callback({status: 1, response: 'database error'});
				}
			});
		}, 
		
		// Check if a user exists (Used for authentication)
		// check(hcwool, password, function(response){});
		check: function($username, $password, callback) {
			var securePassword = shaEncrypt($password);
			User.findOne({username: $username, password: securePassword}, function(err, user){
				if(!err) {
					if(user != null) {
						callback({status: 0, response: user.username});
					}else {
						callback({status: 1, response: 'incorrect credentials'})
					}
				}else {
					callback({status: 1, resposne: 'database error'})
				}
			});
		},
		
		// Update the config of elogin.
		// config({url: 'mongodb://localhost:27017/users', collection: 'user-database', iterations: 11, salt:'xyzboiwhBLEBAULBCL'})
		config: function(options) {
			if(options.url != void(0)) {
				DB_URL = options.url + "";
			}

			if(options.collection != void(0)) {
				COLLECTION = options.collection;
			}

			if(options.iterations != void(0)) {
				ITERATIONS = options.iterations;
			}

			if(options.salt != void(0)) {
				SALT = options.salt;
			}
			console.log('[ELOGIN] SETTINGS: ' + DB_URL + ' : ' + COLLECTION);
			this.setup();
		}
	}
}().setup();